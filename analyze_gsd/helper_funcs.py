# -*- coding: utf-8 -*-
"""
helper_funcs.py

Created on Fri May 27 22:46:14 2016

@author: John Franco Saraceno
@author: saraceno@usgs.gov

This software is in the public domain because it contains materials that 
originally came from the United States Geological Survey,
an agency of the United States Department of Interior. 
For more information, see the official USGS copyright policy at
http://www.usgs.gov/visual-id/credit_usgs.html#copyright

This software is provided "AS IS".
"""
import os
import glob
import matplotlib.pyplot as plt
import pandas as pd


def list_of_files(path, fmatch):
    files = []    
    for name in glob.glob(os.path.join(path, fmatch)):
        if os.path.isfile(os.path.join(path, name)):
            files.append(name)  
    return files

def process_stats(stats_dataframe):    
    """Input: full stats dataframe
    Output: cleaned up and reduced stats dataframe"""
    foo = lambda x: pd.Series([i for i in x.split('= ')])
    stats_df = stats_dataframe.apply(foo)
    field = 'Metric'
    stats_df.rename(columns={0: field, 1: 'Value'}, inplace=True)
    stats_df.index = stats_df[field]
    stats_df.drop(field, inplace=True, axis=1)
    stats_df = stats_df.astype('float')
    stats_df_nzeros = stats_df.loc[(stats_df != 0).any(1)]    
    return stats_df_nzeros.dropna(inplace=False)

def get_gsd(filename):    
    """This function reads in a ls file and returns the vol% gsd as a
    dataframe, with the size as the index"""
    df = pd.read_csv(filename, delimiter='\t', header=None)
    frame = df.copy()
    bindiams = frame.iloc[:, 0].isin(['[#Bindiam]']).idxmax(axis=0, skipna=True)
    sizes = frame.iloc[bindiams+1:bindiams+94, 0].astype('float')
    binheights = frame.iloc[:, 0].isin(['[#Binheight]']).idxmax(axis=0, skipna=True)
    bhs = frame.iloc[binheights+1:binheights+94, 0].astype('float')
    vols = bhs/bhs.sum()*100
    gsd = pd.DataFrame({'Vol %': vols})
    gsd.index = sizes
    gsd.drop(2000, axis=0, inplace=True, errors='raise')
    return gsd

def process_bcc_ls_file(filename):   
    """This function reads in a bcc ls file, grabs and returns a 
    subset of the GSD stats"""
    df = pd.read_csv(filename, delimiter='\t', header=None)
    frame = df.copy()
    stats_start = frame.iloc[:, 0].isin(['[SizeStats]']).idxmax(axis=0, skipna=True)
    stats_end = frame.iloc[:, 0].isin(['[SizePctX]']).idxmax(axis=0, skipna=True)
    stats = frame.iloc[stats_start+1:stats_end, 0]
    return  process_stats(stats)

def plot_bcc_gsd(gsd_dataframe, same_plot=True):
    """This function plots a vol% GSD series/dataframe in log space"""    
    if gsd_dataframe.name:
        label = gsd_dataframe.name
    else:
        label = "Sample"    
    if not same_plot:        
        plt.figure()   # create a new figure    
    plt.plot(gsd_dataframe.index.values, gsd_dataframe.values, lw=2,
             markersize=10, label=label)
    plt.xscale('log')
    plt.ylabel('Volume (%)')
    plt.xlabel('Size ($\mu$m)')
    plt.legend(loc ='best')
    plt.grid('on', which='both', axis='both')
    return