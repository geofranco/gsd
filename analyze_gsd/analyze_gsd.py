# -*- coding: utf-8 -*-
"""
analyze_gsd.py

Created on Fri May 27 22:46:14 2016

@author: John Franco Saraceno
@author: saraceno@usgs.gov

This software is in the public domain because it contains materials that 
originally came from the United States Geological Survey,
an agency of the United States Department of Interior. 
For more information, see the official USGS copyright policy at
http://www.usgs.gov/visual-id/credit_usgs.html#copyright

This software is provided "AS IS".
"""


import os
import pandas as pd
from helper_funcs import get_gsd, list_of_files
from helper_funcs import plot_bcc_gsd, process_bcc_ls_file

    
if __name__ == "__main__":
    
    script_dir = os.path.dirname(__file__) #<-- absolute dir the script is in
    rel_path = "data"
    abs_file_path = os.path.join(script_dir, rel_path)  
    fmatch = '*.$ls'
    files = []
    files = list_of_files(abs_file_path, fmatch)
    
    stats_out_frame = pd.concat((process_bcc_ls_file(f) for f in files), axis=1)
    gsd_out_frame = pd.concat((get_gsd(f) for f in files), axis=1)
    
    sample_names = [] 
    for f in files:
        sample_names.append(f.split(os.sep)[-1].split('.$ls')[0].split(' ')[0])
        
    stats_out_frame.columns = sample_names
    gsd_out_frame.columns = sample_names
    gsd_out_frame.index.name = 'Size (um)'
    #plot output
    for i, col in enumerate(gsd_out_frame.columns):
        plot_bcc_gsd(gsd_out_frame.iloc[:, i], same_plot=True)
   
    saveoutput = True
    if saveoutput:
        stats_out_frame.to_excel(abs_file_path + os.sep + 'sample_gsd_stats.xlsx')
        gsd_out_frame.to_excel(abs_file_path + os.sep + 'sample_gsds.xlsx')
    
