# -*- coding: utf-8 -*-
"""analyze_gsd project"""
from setuptools import setup
from setuptools import find_packages
pkgs = find_packages()

with open('README.*') as f:
    readme = f.read()

with open('LICENSE') as f:
    license = f.read()

setup(name = 'analyze_gsd',
    version = '0.1',
    description = 'analyze_gsd module',
    long_description = readme,
    platforms = ['Windows'],
    author = 'John Franco Saraceno',
    author_email = 'saraceno@usgs.gov',
    url = 'URL to get it at.',
    download_url = 'Where to download it.',
    install_requires = ['nose'],
    scripts = [],
    classifiers = ['Development Status :: 2 - Pre-Alpha'],
    license = license,
    packages = pkgs,
    include_package_data = True,
    )